// parse message fragment in concurrent operation

package main

import (
	"encoding/binary"
	"fmt"
)

var messageStack map[uint64]map[uint16][]byte
var messageRCVD map[uint64]uint16

func initMsgStack() {
	messageStack = make(map[uint64]map[uint16][]byte)
	messageRCVD = make(map[uint64]uint16)
}
func (pcapRecord *pcapRecord) parseFragment(in []byte) {
	curPos := uint32(0)

	blobID := binary.LittleEndian.Uint64(in[curPos : curPos+8])
	numFrags := binary.LittleEndian.Uint16(in[curPos+8 : curPos+10])
	// blobFragSize := uint32(binary.LittleEndian.Uint16(in[curPos+10 : curPos+12]))
	blobNum := binary.LittleEndian.Uint16(in[curPos+12 : curPos+14])
	// queueID := binary.LittleEndian.Uint16(in[curPos+14 : curPos+16])

	if numFrags > 1 {
		//handle multi-fragment messages
		if _, ok := messageStack[blobID]; !ok {
			messageStack[blobID] = make(map[uint16][]byte)
		}
		messageStack[blobID][blobNum] = in[16:]
		messageRCVD[blobID]++
		if messageRCVD[blobID] == numFrags {
			in = []byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
			for i := uint16(0); i < numFrags; i++ {
				in = append(in, messageStack[blobID][i]...)
			}
		} else {
			//don't have all of the frags for this message...
			return
		}
	}
	// fmt.Printf("[Frag: 0x%016X numFrags:%d size:%db queueID:%d (%d)]\n",
	// 	blobID,
	// 	numFrags,
	// 	blobFragSize,
	// 	queueID,
	// 	len(in),
	// 	//getBlock(data[curPos+16:curPos+blobFragSize]),
	// )
	pcapRecord.processMessage(in[16:])
}
func (pcapRecord *pcapRecord) processMessage(in []byte) {
	fmt.Printf("[%6d] Message type 0x%04X %d\n", pcapRecord.Index,	binary.LittleEndian.Uint32(in[0:4]), len(in))
}
