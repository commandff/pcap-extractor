// misc methods used throughout

package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"runtime"
	"strings"
	"time"
)

// configuration file path
var configFile = "./config.json"

// global configuration struct
var c conf

// centralized log function, so all output can be piped to other places
func log(message ...interface{}) {
	pc, fn, line, _ := runtime.Caller(1)
	justfn := strings.Split(fn, "/")
	var b strings.Builder
	fmt.Fprintf(&b, "")
	for it := range message {
		fmt.Fprintf(&b, " %v", message[it])
	}
	fmt.Printf("%.3f [%s(%s):%d]%s\n", float64(ktime())/1000, justfn[len(justfn)-1], runtime.FuncForPC(pc).Name(), line, b.String())
}

// centralized log function, so all output can be piped to other places
func logf(format string, a ...interface{}) {
	pc, fn, line, _ := runtime.Caller(1)
	justfn := strings.Split(fn, "/")
	out := fmt.Sprintf(format, a...)
	fmt.Printf("%.3f [%s(%s):%d] %s\n", float64(ktime())/1000, justfn[len(justfn)-1], runtime.FuncForPC(pc).Name(), line, out)
}

// centralized log function, so all output can be piped to other places
func logq(message string) {
	fmt.Printf("%.3f %s\n", float64(ktime())/1000, message)
}

// time format used throughout
func ktime() int64 { return time.Now().UnixNano() / int64(time.Millisecond) }

// read config.json file
func initConfig() {
	jsonFile, err := os.Open(configFile)
	if err != nil {
		log(err)
		panic(err)
	}
	defer jsonFile.Close()
	byteValue, errr := ioutil.ReadAll(jsonFile)
	if errr != nil {
		log(err)
		panic(err)
	}
	if err := json.Unmarshal(byteValue, &c); err != nil {
		log(err)
		panic(err)
	}
	logf("Read %db from %s", len(byteValue), configFile)
	byteValue = nil
}

type conf struct {
	SQL map[string]string `json:"sql"`
}

func max(v1 int64, v2 int64) int64 {
	if v1 > v2 {
		return v1
	}
	return v2
}
func min(v1 int64, v2 int64) int64 {
	if v1 < v2 {
		return v1
	}
	return v2
}
func byt(b uint64) string {
	const unit = 1024
	if b < unit {
		return fmt.Sprintf("%db", b)
	}
	div, exp := int64(unit), 0
	for n := b / unit; n >= unit; n /= unit {
		div *= unit
		exp++
	}
	return fmt.Sprintf("%s%cb", bytFormat(float64(b)/float64(div)), "kmgtpe"[exp])
}
func bytn(b uint64) string {
	const unit = 1000
	if b < unit {
		return fmt.Sprintf("%d", b)
	}
	div, exp := int64(unit), 0
	for n := b / unit; n >= unit; n /= unit {
		div *= unit
		exp++
	}
	return fmt.Sprintf("%s%c", bytFormat(float64(b)/float64(div)), "kmgtpe"[exp])
}
func bytFormat(num float64) string {
	if num < 9.5 {
		return strings.TrimRight(strings.TrimRight(fmt.Sprintf("%.2f", num), "0"), ".")
	}
	if num < 99.5 {
		return strings.TrimRight(strings.TrimRight(fmt.Sprintf("%.1f", num), "0"), ".")
	}
	return fmt.Sprintf("%.0f", num)

}
