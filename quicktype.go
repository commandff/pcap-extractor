// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse and unparse this JSON data, add this code to your project and do:
//
//    messages, err := UnmarshalMessages(bytes)
//    bytes, err = messages.Marshal()

package main

import "bytes"
import "errors"
import "encoding/json"

func UnmarshalMessages(data []byte) (Messages, error) {
	var r Messages
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Messages) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type Messages struct {
	Schema Schema `json:"schema"`
}

type Schema struct {
	Revision  Revision      `json:"revision"` 
	Datatypes Datatypes     `json:"datatypes"`
	Messages  MessagesClass `json:"messages"` 
	Version   string        `json:"_version"` 
}

type Datatypes struct {
	Type []Type `json:"type"`
}

type Type struct {
	Name      Name           `json:"_name"`               
	Primitive *string        `json:"_primitive,omitempty"`
	Field     []FieldElement `json:"field"`               
	Maskmap   *MaskmapUnion  `json:"maskmap"`             
	Vector    *TypeVector    `json:"vector"`              
	Align     *AlignUnion    `json:"align"`               
}

type AlignElement struct {
	Type Name `json:"_type"`
}

type FieldElement struct {
	Name string `json:"_name"`
	Type Name   `json:"_type"`
}

type MaskmapElement struct {
	Mask *HilariousMask `json:"mask"`          
	Name string         `json:"_name"`         
	Xor  *string        `json:"_xor,omitempty"`
}

type PurpleMask struct {
	Field  *FieldUnion   `json:"field"`           
	Value  string        `json:"_value"`          
	Vector *PurpleVector `json:"vector,omitempty"`
	Align  *AlignElement `json:"align,omitempty"` 
}

type PurpleVector struct {
	Field  *FieldUnion   `json:"field"`           
	Name   string        `json:"_name"`           
	Length string        `json:"_length"`         
	Switch *PurpleSwitch `json:"switch,omitempty"`
	Vector *FluffyVector `json:"vector,omitempty"`
}

type PurpleSwitch struct {
	Case []PurpleCase `json:"case"` 
	Name string       `json:"_name"`
}

type PurpleCase struct {
	Field  []FieldElement `json:"field"`           
	Vector *FluffyVector  `json:"vector,omitempty"`
	Value  string         `json:"_value"`          
}

type FluffyVector struct {
	Field  FieldElement  `json:"field"`           
	Switch *FluffySwitch `json:"switch,omitempty"`
	Name   string        `json:"_name"`           
	Length string        `json:"_length"`         
}

type FluffySwitch struct {
	Case FluffyCase `json:"case"` 
	Name string     `json:"_name"`
}

type FluffyCase struct {
	Field  []FieldElement  `json:"field"` 
	Vector TentacledVector `json:"vector"`
	Value  string          `json:"_value"`
}

type TentacledVector struct {
	Field  FieldElement    `json:"field"`  
	Switch TentacledSwitch `json:"switch"` 
	Name   string          `json:"_name"`  
	Length string          `json:"_length"`
}

type TentacledSwitch struct {
	Case []TentacledCase `json:"case"` 
	Name string          `json:"_name"`
}

type TentacledCase struct {
	Field  []FieldElement `json:"field"`           
	Switch *StickySwitch  `json:"switch,omitempty"`
	Value  string         `json:"_value"`          
}

type StickySwitch struct {
	Case []StickyCase `json:"case"` 
	Name string       `json:"_name"`
}

type StickyCase struct {
	Field *FieldUnion `json:"field"` 
	Value string      `json:"_value"`
}

type FluffyMask struct {
	Field  FieldElement   `json:"field"`           
	Value  string         `json:"_value"`          
	Vector *MessageVector `json:"vector,omitempty"`
}

type MessageVector struct {
	Field  []FieldElement `json:"field"`  
	Name   string         `json:"_name"`  
	Length string         `json:"_length"`
	Vector []FluffyVector `json:"vector"` 
}

type PurpleMaskmap struct {
	Mask *AmbitiousMask `json:"mask"` 
	Name string         `json:"_name"`
}

type TentacledMask struct {
	Field   *FieldUnion    `json:"field"`            
	Vector  *PurpleVector  `json:"vector,omitempty"` 
	Value   string         `json:"_value"`           
	Maskmap *FluffyMaskmap `json:"maskmap,omitempty"`
}

type FluffyMaskmap struct {
	Mask []StickyMask `json:"mask"` 
	Name string       `json:"_name"`
}

type StickyMask struct {
	Field  FieldElement  `json:"field"`           
	Value  string        `json:"_value"`          
	Vector *FluffyVector `json:"vector,omitempty"`
}

type CaseElement struct {
	Field *FieldElement `json:"field,omitempty"`
	Value string        `json:"_value"`         
}

type MessagesClass struct {
	Message []Message `json:"message"`
}

type Message struct {
	Field     *FieldUnion     `json:"field"`               
	Type      string          `json:"_type"`               
	Align     *AlignElement   `json:"align,omitempty"`     
	Direction *string         `json:"_direction,omitempty"`
	Vector    *MessageVector  `json:"vector,omitempty"`    
	Switch    *MessageSwitch  `json:"switch,omitempty"`    
	Maskmap   *MessageMaskmap `json:"maskmap,omitempty"`   
}

type MessageMaskmap struct {
	Mask []CaseElement `json:"mask"` 
	Name string        `json:"_name"`
}

type MessageSwitch struct {
	Case []IndigoCase `json:"case"` 
	Name string       `json:"_name"`
}

type IndigoCase struct {
	Field   *FieldUnion      `json:"field"`            
	Maskmap *CaseMaskmap     `json:"maskmap,omitempty"`
	Vector  *CaseVectorUnion `json:"vector"`           
	Value   string           `json:"_value"`           
	Switch  *IndigoSwitch    `json:"switch,omitempty"` 
}

type CaseMaskmap struct {
	Mask []IndigoMask `json:"mask"` 
	Name string       `json:"_name"`
}

type IndigoMask struct {
	Field   *FieldUnion       `json:"field"`            
	Value   string            `json:"_value"`           
	Vector  *MessageVector    `json:"vector,omitempty"` 
	Maskmap *TentacledMaskmap `json:"maskmap,omitempty"`
}

type TentacledMaskmap struct {
	Mask []IndecentMask `json:"mask"` 
	Name string         `json:"_name"`
}

type IndecentMask struct {
	Field []FieldElement `json:"field"` 
	Value string         `json:"_value"`
}

type IndigoSwitch struct {
	Case CaseElement `json:"case"` 
	Name string      `json:"_name"`
}

type StickyVector struct {
	Field  *FieldUnion    `json:"field"`          
	Name   string         `json:"_name"`          
	Length string         `json:"_length"`        
	Mask   *string        `json:"_mask,omitempty"`
	Align  *AlignElement  `json:"align,omitempty"`
	Vector []FluffyVector `json:"vector"`         
	Skip   *string        `json:"_skip,omitempty"`
}

type Revision struct {
	Version string `json:"_version"`
}

type Name string
const (
	AttributeData Name = "AttributeData"
	Byte Name = "BYTE"
	CharacterOptionData Name = "CharacterOptionData"
	CharacterPropertyData Name = "CharacterPropertyData"
	CharacterVectorData Name = "CharacterVectorData"
	Double Name = "double"
	DwellingACL Name = "DwellingACL"
	DwellingItem Name = "DwellingItem"
	Dword Name = "DWORD"
	Enchantment Name = "Enchantment"
	FellowInfo Name = "FellowInfo"
	Float Name = "float"
	GameData Name = "GameData"
	ModelData Name = "ModelData"
	PackedDWORD Name = "PackedDWORD"
	PackedWORD Name = "PackedWORD"
	PhysicsData Name = "PhysicsData"
	Position Name = "Position"
	Position0 Name = "Position0"
	Qword Name = "QWORD"
	SkillData Name = "SkillData"
	String Name = "String"
	VitalData Name = "VitalData"
	WString Name = "WString"
	Word Name = "WORD"
)

type AlignUnion struct {
	AlignElement      *AlignElement
	AlignElementArray []AlignElement
}

func (x *AlignUnion) UnmarshalJSON(data []byte) error {
	x.AlignElementArray = nil
	x.AlignElement = nil
	var c AlignElement
	object, err := unmarshalUnion(data, nil, nil, nil, nil, true, &x.AlignElementArray, true, &c, false, nil, false, nil, false)
	if err != nil {
		return err
	}
	if object {
		x.AlignElement = &c
	}
	return nil
}

func (x *AlignUnion) MarshalJSON() ([]byte, error) {
	return marshalUnion(nil, nil, nil, nil, x.AlignElementArray != nil, x.AlignElementArray, x.AlignElement != nil, x.AlignElement, false, nil, false, nil, false)
}

type MaskmapUnion struct {
	MaskmapElementArray []MaskmapElement
	PurpleMaskmap       *PurpleMaskmap
}

func (x *MaskmapUnion) UnmarshalJSON(data []byte) error {
	x.MaskmapElementArray = nil
	x.PurpleMaskmap = nil
	var c PurpleMaskmap
	object, err := unmarshalUnion(data, nil, nil, nil, nil, true, &x.MaskmapElementArray, true, &c, false, nil, false, nil, false)
	if err != nil {
		return err
	}
	if object {
		x.PurpleMaskmap = &c
	}
	return nil
}

func (x *MaskmapUnion) MarshalJSON() ([]byte, error) {
	return marshalUnion(nil, nil, nil, nil, x.MaskmapElementArray != nil, x.MaskmapElementArray, x.PurpleMaskmap != nil, x.PurpleMaskmap, false, nil, false, nil, false)
}

type HilariousMask struct {
	FluffyMask      *FluffyMask
	PurpleMaskArray []PurpleMask
}

func (x *HilariousMask) UnmarshalJSON(data []byte) error {
	x.PurpleMaskArray = nil
	x.FluffyMask = nil
	var c FluffyMask
	object, err := unmarshalUnion(data, nil, nil, nil, nil, true, &x.PurpleMaskArray, true, &c, false, nil, false, nil, false)
	if err != nil {
		return err
	}
	if object {
		x.FluffyMask = &c
	}
	return nil
}

func (x *HilariousMask) MarshalJSON() ([]byte, error) {
	return marshalUnion(nil, nil, nil, nil, x.PurpleMaskArray != nil, x.PurpleMaskArray, x.FluffyMask != nil, x.FluffyMask, false, nil, false, nil, false)
}

type FieldUnion struct {
	FieldElement      *FieldElement
	FieldElementArray []FieldElement
}

func (x *FieldUnion) UnmarshalJSON(data []byte) error {
	x.FieldElementArray = nil
	x.FieldElement = nil
	var c FieldElement
	object, err := unmarshalUnion(data, nil, nil, nil, nil, true, &x.FieldElementArray, true, &c, false, nil, false, nil, false)
	if err != nil {
		return err
	}
	if object {
		x.FieldElement = &c
	}
	return nil
}

func (x *FieldUnion) MarshalJSON() ([]byte, error) {
	return marshalUnion(nil, nil, nil, nil, x.FieldElementArray != nil, x.FieldElementArray, x.FieldElement != nil, x.FieldElement, false, nil, false, nil, false)
}

type AmbitiousMask struct {
	CaseElement        *CaseElement
	TentacledMaskArray []TentacledMask
}

func (x *AmbitiousMask) UnmarshalJSON(data []byte) error {
	x.TentacledMaskArray = nil
	x.CaseElement = nil
	var c CaseElement
	object, err := unmarshalUnion(data, nil, nil, nil, nil, true, &x.TentacledMaskArray, true, &c, false, nil, false, nil, false)
	if err != nil {
		return err
	}
	if object {
		x.CaseElement = &c
	}
	return nil
}

func (x *AmbitiousMask) MarshalJSON() ([]byte, error) {
	return marshalUnion(nil, nil, nil, nil, x.TentacledMaskArray != nil, x.TentacledMaskArray, x.CaseElement != nil, x.CaseElement, false, nil, false, nil, false)
}

type TypeVector struct {
	MessageVector     *MessageVector
	PurpleVectorArray []PurpleVector
}

func (x *TypeVector) UnmarshalJSON(data []byte) error {
	x.PurpleVectorArray = nil
	x.MessageVector = nil
	var c MessageVector
	object, err := unmarshalUnion(data, nil, nil, nil, nil, true, &x.PurpleVectorArray, true, &c, false, nil, false, nil, false)
	if err != nil {
		return err
	}
	if object {
		x.MessageVector = &c
	}
	return nil
}

func (x *TypeVector) MarshalJSON() ([]byte, error) {
	return marshalUnion(nil, nil, nil, nil, x.PurpleVectorArray != nil, x.PurpleVectorArray, x.MessageVector != nil, x.MessageVector, false, nil, false, nil, false)
}

type CaseVectorUnion struct {
	PurpleVectorArray []PurpleVector
	StickyVector      *StickyVector
}

func (x *CaseVectorUnion) UnmarshalJSON(data []byte) error {
	x.PurpleVectorArray = nil
	x.StickyVector = nil
	var c StickyVector
	object, err := unmarshalUnion(data, nil, nil, nil, nil, true, &x.PurpleVectorArray, true, &c, false, nil, false, nil, false)
	if err != nil {
		return err
	}
	if object {
		x.StickyVector = &c
	}
	return nil
}

func (x *CaseVectorUnion) MarshalJSON() ([]byte, error) {
	return marshalUnion(nil, nil, nil, nil, x.PurpleVectorArray != nil, x.PurpleVectorArray, x.StickyVector != nil, x.StickyVector, false, nil, false, nil, false)
}

func unmarshalUnion(data []byte, pi **int64, pf **float64, pb **bool, ps **string, haveArray bool, pa interface{}, haveObject bool, pc interface{}, haveMap bool, pm interface{}, haveEnum bool, pe interface{}, nullable bool) (bool, error) {
	if pi != nil {
		*pi = nil
	}
	if pf != nil {
		*pf = nil
	}
	if pb != nil {
		*pb = nil
	}
	if ps != nil {
		*ps = nil
	}

	dec := json.NewDecoder(bytes.NewReader(data))
	dec.UseNumber()
	tok, err := dec.Token()
	if err != nil {
		return false, err
	}

	switch v := tok.(type) {
	case json.Number:
		if pi != nil {
			i, err := v.Int64()
			if err == nil {
				*pi = &i
				return false, nil
			}
		}
		if pf != nil {
			f, err := v.Float64()
			if err == nil {
				*pf = &f
				return false, nil
			}
			return false, errors.New("Unparsable number")
		}
		return false, errors.New("Union does not contain number")
	case float64:
		return false, errors.New("Decoder should not return float64")
	case bool:
		if pb != nil {
			*pb = &v
			return false, nil
		}
		return false, errors.New("Union does not contain bool")
	case string:
		if haveEnum {
			return false, json.Unmarshal(data, pe)
		}
		if ps != nil {
			*ps = &v
			return false, nil
		}
		return false, errors.New("Union does not contain string")
	case nil:
		if nullable {
			return false, nil
		}
		return false, errors.New("Union does not contain null")
	case json.Delim:
		if v == '{' {
			if haveObject {
				return true, json.Unmarshal(data, pc)
			}
			if haveMap {
				return false, json.Unmarshal(data, pm)
			}
			return false, errors.New("Union does not contain object")
		}
		if v == '[' {
			if haveArray {
				return false, json.Unmarshal(data, pa)
			}
			return false, errors.New("Union does not contain array")
		}
		return false, errors.New("Cannot handle delimiter")
	}
	return false, errors.New("Cannot unmarshal union")

}

func marshalUnion(pi *int64, pf *float64, pb *bool, ps *string, haveArray bool, pa interface{}, haveObject bool, pc interface{}, haveMap bool, pm interface{}, haveEnum bool, pe interface{}, nullable bool) ([]byte, error) {
	if pi != nil {
		return json.Marshal(*pi)
	}
	if pf != nil {
		return json.Marshal(*pf)
	}
	if pb != nil {
		return json.Marshal(*pb)
	}
	if ps != nil {
		return json.Marshal(*ps)
	}
	if haveArray {
		return json.Marshal(pa)
	}
	if haveObject {
		return json.Marshal(pc)
	}
	if haveMap {
		return json.Marshal(pm)
	}
	if haveEnum {
		return json.Marshal(pe)
	}
	if nullable {
		return json.Marshal(nil)
	}
	return nil, errors.New("Union must not be null")
}
