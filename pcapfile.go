// parse pcap file, return object

package main

import (
	"encoding/binary"
	"fmt"
	"io/ioutil"
	"math"
	"os"
	"strings"
)

type pcap struct {
	Filename     string
	Valid        bool
	FileSize     uint32
	MagicNumber  uint32
	VersionMajor uint16
	VersionMinor uint16
	Network      uint32
	record       []pcapRecord
}

type pcapRecord struct {
	Index      uint32
	TsSec      uint32
	TsUsec     uint32
	InclLen    uint32
	SrcIP      []byte
	SrcPort    uint16
	DstIP      []byte
	DstPort    uint16
	dwSequence uint32
	dwFlags    uint32
	dwCRC      uint32
	wRecID     uint16
	wTime      uint16
	wSize      uint16
	wTable     uint16
	// Data    []byte
}

func loadPcap(file string) pcap {
	var err error

	ret := pcap{
		Filename: file,
		Valid:    false,
	}

	pcapFile, err := os.Open(file)
	if err != nil {
		log(err)
		return ret
	}

	data, err := ioutil.ReadAll(pcapFile)
	pcapFile.Close()
	if err != nil {
		log(err)
		return ret
	}
	ret.FileSize = uint32(len(data))

	if ret.FileSize < 25 {
		log("insufficient file size")
		return ret
	}
	curPos := uint32(0)
	// https://wiki.wireshark.org/Development/LibpcapFileFormat#Global_Header
	ret.MagicNumber = binary.LittleEndian.Uint32(data[0:4])

	if ret.MagicNumber == 0xA1B2C3D4 {
		ret.VersionMajor = binary.LittleEndian.Uint16(data[4:6])
		ret.VersionMinor = binary.LittleEndian.Uint16(data[6:8])
		ret.Network = binary.LittleEndian.Uint32(data[20:24])
	} else if ret.MagicNumber == 0xD4C3B2A1 {
		ret.VersionMajor = binary.BigEndian.Uint16(data[4:6])
		ret.VersionMinor = binary.BigEndian.Uint16(data[6:8])
		ret.Network = binary.BigEndian.Uint32(data[20:24])
	} else {
		log("invalid pcap file")
		return ret
	}

	if ret.Network != 1 {
		log("invalid link layer type")
		return ret
	}
	ret.Valid = true
	logf("Found magic number 0x%04X v%d.%d, Network %d",
		ret.MagicNumber,
		ret.VersionMajor,
		ret.VersionMinor,
		ret.Network,
	)
	if curPos == 0 {
		curPos = 24
	}
	idx := uint32(0)
	thisPacket := uint32(0)
	nextPacket := uint32(0)
	for curPos < ret.FileSize {
		// https://wiki.wireshark.org/Development/LibpcapFileFormat#Record_.28Packet.29_Header
		rec := pcapRecord{
			Index:   idx,
			TsSec:   binary.LittleEndian.Uint32(data[curPos : curPos+4]),
			TsUsec:  binary.LittleEndian.Uint32(data[curPos+4 : curPos+8]),
			InclLen: binary.LittleEndian.Uint32(data[curPos+8 : curPos+12]),
		}

		curPos += 16
		nextPacket = curPos + rec.InclLen
		thisPacket = curPos

		//start ethernet header
		// https://en.wikipedia.org/wiki/Ethernet_frame#Ethernet_II
		//verify ethernet type is IP:
		if binary.LittleEndian.Uint16(data[curPos+12:curPos+14]) != 8 {
			log("Not IP!")
			curPos = nextPacket
			continue
		}
		// etherCRC := binary.LittleEndian.Uint32(data[curPos+(rec.InclLen-4) : curPos+rec.InclLen])
		curPos += 14

		//start ip header
		// https://en.wikipedia.org/wiki/IPv4#Packet_structure
		ipHeaderLength := uint32((data[curPos] & 0x0F) << 2) //I see you staring at me. stop it.

		//verify proto:
		if data[curPos+9] != 17 {
			log("Not UDP!")
			curPos = nextPacket
			continue
		}
		rec.SrcIP = data[curPos+12 : curPos+16]
		rec.DstIP = data[curPos+16 : curPos+20]
		curPos += ipHeaderLength //jump to end of ip header

		//start udp header
		// https://en.wikipedia.org/wiki/User_Datagram_Protocol#Packet_structure
		rec.SrcPort = binary.BigEndian.Uint16(data[curPos : curPos+2])
		rec.DstPort = binary.BigEndian.Uint16(data[curPos+2 : curPos+4])

		dir := ""
		server := ""
		if rec.DstPort >= 9000 && rec.DstPort <= 10013 && rec.SrcIP[0] == 127 {
			dir = "C2S"
			server = fmt.Sprintf("%d.%d.%d.%d:%d", rec.DstIP[0], rec.DstIP[1], rec.DstIP[2], rec.DstIP[3], rec.DstPort)
		} else if rec.SrcPort >= 9000 && rec.SrcPort <= 10013 && rec.DstIP[0] == 127 {
			dir = "S2C"
			server = fmt.Sprintf("%d.%d.%d.%d:%d", rec.SrcIP[0], rec.SrcIP[1], rec.SrcIP[2], rec.SrcIP[3], rec.SrcPort)
		} else {
			log("Not UDP!")
			curPos = nextPacket
			continue
		}

		curPos += 8
		thisPacket = curPos

		//start AC packet processing.

		rec.dwSequence = binary.LittleEndian.Uint32(data[curPos : curPos+4])
		rec.dwFlags = binary.LittleEndian.Uint32(data[curPos+4 : curPos+8])
		rec.dwCRC = binary.LittleEndian.Uint32(data[curPos+8 : curPos+12])
		rec.wRecID = binary.LittleEndian.Uint16(data[curPos+12 : curPos+14])
		rec.wTime = binary.LittleEndian.Uint16(data[curPos+14 : curPos+16])
		rec.wSize = binary.LittleEndian.Uint16(data[curPos+16 : curPos+18])
		rec.wTable = binary.LittleEndian.Uint16(data[curPos+18 : curPos+20])

		var acpacket strings.Builder

		acpacket.WriteString(fmt.Sprintf("Packet %4d %d.%06d %s[%s]: sequence: 0x%08X flags: 0x%016X CRC:%08X RecID:%4d time:%4d size:%3d table:%d",
			rec.Index,
			rec.TsSec,
			rec.TsUsec,
			dir, server,
			rec.dwSequence,
			rec.dwFlags,
			rec.dwCRC,
			rec.wRecID,
			rec.wTime,
			rec.wSize,
			rec.wTable,
		))
		curPos += 20
		if (rec.dwFlags & 0x2) != 0 {
			acpacket.WriteString("[Encrypted CRC]")
		}
		if (rec.dwFlags & 0x100) != 0 {
			acpacket.WriteString(fmt.Sprintf("[Server Switch 0x%08X %08X]", binary.LittleEndian.Uint32(data[curPos:curPos+4]), binary.LittleEndian.Uint32(data[curPos+4:curPos+8])))
			curPos += 8
		}
		if (rec.dwFlags & 0x200) != 0 {
			acpacket.WriteString("[Logon Server]")
		}
		if (rec.dwFlags & 0x400) != 0 {
			acpacket.WriteString("[Empty1]")
		}
		if (rec.dwFlags & 0x800) != 0 {
			acpacket.WriteString(fmt.Sprintf("[Referral 0x%016X %d.%d.%d.%d:%d id 0x%04X]",
				binary.LittleEndian.Uint64(data[curPos:curPos+8]),
				data[curPos+12], data[curPos+13], data[curPos+14], data[curPos+15],
				binary.BigEndian.Uint16(data[curPos+10:curPos+12]),
				binary.LittleEndian.Uint16(data[curPos+24:curPos+26]),
			))
			curPos += 32
		}
		if (rec.dwFlags & 0x1000) != 0 {
			nakPacketCount := binary.LittleEndian.Uint32(data[curPos : curPos+4])
			curPos += 4
			acpacket.WriteString(fmt.Sprintf("[Non Ack x%d:", nakPacketCount))
			for nakPacketCount > 0 {
				nakPacket := binary.LittleEndian.Uint32(data[curPos : curPos+4])
				acpacket.WriteString(fmt.Sprintf(" 0x%08X", nakPacket))
				curPos += 4
				nakPacketCount--
			}
			acpacket.WriteString("]")
		}
		if (rec.dwFlags & 0x2000) != 0 {
			//	acpacket.WriteString("[Empty Ack]")
			nakPacketCount := binary.LittleEndian.Uint32(data[curPos : curPos+4])
			curPos += 4
			acpacket.WriteString(fmt.Sprintf("[Empty Ack x%d:", nakPacketCount))
			for nakPacketCount > 0 {
				nakPacket := binary.LittleEndian.Uint32(data[curPos : curPos+4])
				acpacket.WriteString(fmt.Sprintf(" 0x%08X", nakPacket))
				curPos += 4
				nakPacketCount--
			}
			acpacket.WriteString("]")
		}
		if (rec.dwFlags & 0x4000) != 0 {
			acpacket.WriteString(fmt.Sprintf("[ACK 0x%08X]", binary.LittleEndian.Uint32(data[curPos:curPos+4])))
			curPos += 4
		}
		if (rec.dwFlags & 0x8000) != 0 {
			acpacket.WriteString("[Empty2]")
		}
		if (rec.dwFlags & 0x10000) != 0 {
			acpacket.WriteString("[LOGIN]")
			curPos = nextPacket
		}
		if (rec.dwFlags & 0x20000) != 0 {
			acpacket.WriteString(fmt.Sprintf("[ULong 0x%016X]", binary.LittleEndian.Uint64(data[curPos:curPos+8])))
			curPos += 8
		}
		if (rec.dwFlags & 0x40000) != 0 { // Connect
			acpacket.WriteString(fmt.Sprintf("[Connect Time:%f Cookie:0x%016X Net:%08X SeedOut:%08X SeedIn:%08X]",
				math.Float64frombits(binary.LittleEndian.Uint64(data[curPos:curPos+8])),
				binary.LittleEndian.Uint64(data[curPos+8:curPos+16]),
				binary.LittleEndian.Uint32(data[curPos+16:curPos+20]),
				binary.LittleEndian.Uint32(data[curPos+20:curPos+24]),
				binary.LittleEndian.Uint32(data[curPos+24:curPos+28]),
			))
			curPos += 32

		}

		if (rec.dwFlags & 0x80000) != 0 {
			acpacket.WriteString(fmt.Sprintf("[ULong2 0x%016X]", binary.LittleEndian.Uint64(data[curPos:curPos+8])))
			curPos += 8
		}

		if (rec.dwFlags & 0x100000) != 0 {
			acpacket.WriteString(fmt.Sprintf("[Net ERR string:0x%08X table:%08X]", binary.LittleEndian.Uint32(data[curPos:curPos+4]), binary.LittleEndian.Uint32(data[curPos+4:curPos+8])))
			curPos += 8
		}

		if (rec.dwFlags & 0x200000) != 0 {
			acpacket.WriteString(fmt.Sprintf("[Net DISCONNECT 0x%016X]", binary.LittleEndian.Uint64(data[curPos:curPos+8])))
			curPos += 8
		}

		if (rec.dwFlags & 0x400000) != 0 {
			acpacket.WriteString(fmt.Sprintf("[CMD 0x%08X %08X]", binary.LittleEndian.Uint32(data[curPos:curPos+4]), binary.LittleEndian.Uint32(data[curPos+4:curPos+8])))
			curPos += 8
		}

		if (rec.dwFlags & 0x1000000) != 0 {
			acpacket.WriteString(fmt.Sprintf("[Time Sync 0x%016X]", binary.LittleEndian.Uint64(data[curPos:curPos+8])))
			curPos += 8
		}

		if (rec.dwFlags & 0x2000000) != 0 {
			acpacket.WriteString(fmt.Sprintf("[Echo Req %f]",
				math.Float32frombits(binary.LittleEndian.Uint32(data[curPos:curPos+4])),
			))
			curPos += 4
		}

		if (rec.dwFlags & 0x4000000) != 0 {
			acpacket.WriteString(fmt.Sprintf("[Echo Resp %f %f]",
				math.Float32frombits(binary.LittleEndian.Uint32(data[curPos:curPos+4])),
				math.Float32frombits(binary.LittleEndian.Uint32(data[curPos+4:curPos+8])),
			))
			curPos += 8
		}

		if (rec.dwFlags & 0x8000000) != 0 {
			acpacket.WriteString(fmt.Sprintf("[Flow 0x%08X 0x%04X]",
				binary.LittleEndian.Uint32(data[curPos:curPos+4]),
				binary.LittleEndian.Uint16(data[curPos+4:curPos+6]),
			))
			curPos += 6
		}
		if (rec.dwFlags & 0x4) != 0 {
			for curPos != nextPacket {
				if curPos+15 > nextPacket {
					fmt.Printf("%s\n", acpacket.String())
					dumpBlock(data[thisPacket:nextPacket])
					panic("I dun made an oopsie.")
				}
				blobID := binary.LittleEndian.Uint64(data[curPos : curPos+8])
				numFrags := binary.LittleEndian.Uint16(data[curPos+8 : curPos+10])
				blobFragSize := uint32(binary.LittleEndian.Uint16(data[curPos+10 : curPos+12]))
				blobNum := binary.LittleEndian.Uint16(data[curPos+12 : curPos+14])
				queueID := binary.LittleEndian.Uint16(data[curPos+14 : curPos+16])
				acpacket.WriteString(fmt.Sprintf("[Frag: 0x%016X numFrags:%d size:%db blobNum:%d queueID:%d]",
					blobID,
					numFrags,
					blobFragSize,
					blobNum,
					queueID,
				))
				rec.parseFragment(data[curPos : curPos+blobFragSize])
				curPos += blobFragSize
			}
		}
		if nextPacket == curPos {
			//fmt.Printf("%s\n", acpacket.String())
		} else {
			acpacket.WriteString(fmt.Sprintf("\n\n[%db remaining]", nextPacket-curPos))
			fmt.Printf("%s\n", acpacket.String())
			dumpBlock(data[curPos:nextPacket])
		}

		curPos = nextPacket
		idx++
	}
	logf("Read %db from %s", ret.FileSize, ret.Filename)

	return ret
}
func dumpBlock(in []byte) {
	var outp strings.Builder
	for i := range in {
		if i%4 == 0 {
			if i%16 == 0 {
				if i > 0 {
					outp.WriteString("\n")
				}
				outp.WriteString(fmt.Sprintf("%4d: ", i))
			} else {
				outp.WriteString(" ")
			}
		}
		outp.WriteString(fmt.Sprintf("%02X", in[i]))
	}
	fmt.Printf("%s\n", outp.String())
}

func getBlock(in []byte) string {
	var outp strings.Builder
	for i := range in {
		if i > 0 && i%4 == 0 {
			outp.WriteString(" ")
		}
		outp.WriteString(fmt.Sprintf("%02X", in[i]))
	}
	return outp.String()
}
